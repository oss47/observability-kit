Tools Kit for observable services.

## How to use it

### Configure tracing

1. Simple scenario with environment based configuration and plain output, ideal for Rust tests:

```rust
    let _result = observability_kit::tracing::init();
```

2. Open Telemetry Protocol configured tracing.

```rust
    let _result = observability_kit::otlp_tracing::try_init(env!("CARGO_PKG_NAME"));
```

You can use `docker-compose up -d` to run [Jaeger](https://www.jaegertracing.io/) and see OTLP
in action.
